import React, { useState, useEffect } from "react";

const SaleForm = () => {
  const [salesPeople, setSalesPeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);

  const [formData, setFormData] = useState({
    price: "",
    automobile: "",
    salesperson: "",
    customer: "",
  });

  const fetchSalesPeople = async () => {
    const salesPeopleUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salesPeopleUrl);
    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.salespeople);
    }
  };

  useEffect(() => {
    fetchSalesPeople();
  }, []);

  const fetchCustomers = async () => {
    const customersUrl = "http://localhost:8090/api/customers/";
    const response = await fetch(customersUrl);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  useEffect(() => {
    fetchCustomers();
  }, []);

  const fetchAutomobiles = async () => {
    const automobilesUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(automobilesUrl);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const saleUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(saleUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        price: "",
        automobile: "",
        salesperson: "",
        customer: "",
      });
      alert("Sale created successfully!");
    }
  };

  const handleChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };
  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="price">Price</label>
        <input
          value={formData.price}
          onChange={handleChange}
          required
          type="number"
          className="form-control"
          id="price"
          name="price"
          placeholder="Price"
        />
      </div>
      <div className="form-group">
        <label htmlFor="automobile">Automobile</label>
        <select
          value={formData.data}
          onChange={handleChange}
          required
          className="form-control"
          id="automobile"
          name="automobile"
        >
          <option value="">Select an automobile</option>
          {automobiles.filter((auto) => !auto.sold)
          .map((auto) => (
            <option key={auto["vin"]} value={auto["vin"]}>
              {auto["year"]} {auto["model"]["manufacturer"]["name"]} {auto["model"]["name"]} - {auto["vin"]}
            </option>
          ))}
        </select>
      </div>
      <div className="form-group">
        <label htmlFor="salesperson">Salesperson</label>
        <select
          value={formData.data}
          onChange={handleChange}
          required
          className="form-control"
          id="salesperson"
          name="salesperson"
        >
          <option value="">Select a salesperson</option>
          {salesPeople.map((salesPerson) => (
            <option
              key={salesPerson["Django ID"]}
              value={salesPerson["Django ID"]}
            >
              {salesPerson.employee_id} - {salesPerson.first_name}{" "}
              {salesPerson.last_name}
            </option>
          ))}
        </select>
      </div>
      <div className="form-group">
        <label htmlFor="customer">Customer</label>
        <select
          value={formData.data}
          onChange={handleChange}
          required
          className="form-control"
          id="customer"
          name="customer"
        >
          <option value="">Select a customer</option>
          {customers.map((customer) => (
            <option
              key={customer["Customer ID"]}
              value={customer["Customer ID"]}
            >
              {customer["Customer ID"]} - {customer.first_name}{" "}
              {customer.last_name}
            </option>
          ))}
        </select>
      </div>
      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
};

export default SaleForm;
