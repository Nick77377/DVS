import React, { useEffect, useState } from "react";

const CustomerForm = () => {
    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        phone_number: "",
        address: "",
});

    const handleSubmit = async (e) => {
        e.preventDefault();
        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
        },
    };

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
        setFormData({
            first_name: "",
            last_name: "",
            phone_number: "",
            address: "",
        });
        alert("Customer created successfully!");
    }
};

    const handleChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
             ...formData,
             [inputName]: value
        });
    };

    return (
        <form onSubmit={handleSubmit}>
            <div className ="form-group">
                <label htmlFor="first_name">First Name</label>
                <input
                    value={formData.first_name}
                    onChange={handleChange}
                    required
                    type="text"
                    className="form-control"
                    id="first_name"
                    name="first_name"
                    placeholder="First Name"
                />
            </div>
            <div className ="form-group">
                <label htmlFor="last_name">Last Name</label>
                <input
                    value={formData.last_name}
                    onChange={handleChange}
                    required
                    type="text"
                    className="form-control"
                    id="last_name"
                    name="last_name"
                    placeholder="Last Name"
                />
            </div>
            <div className ="form-group">
                <label htmlFor="phone_number">Phone Number</label>
                <input
                    value={formData.phone_number}
                    onChange={handleChange}
                    required
                    type="text"
                    className="form-control"
                    id="phone_number"
                    name="phone_number"
                    placeholder="Phone Number"
                />
            </div>
            <div className ="form-group">
                <label htmlFor="address">Address</label>
                <input
                    value={formData.address}
                    onChange={handleChange}
                    required
                    type="text"
                    className="form-control"
                    id="address"
                    name="address"
                    placeholder="Address"
                />
            </div>
            <button type="submit" className="btn btn-primary">
                Create
            </button>
        </form>
    );
};

export default CustomerForm;
