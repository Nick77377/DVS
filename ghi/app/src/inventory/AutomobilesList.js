import React, { useState, useEffect } from "react";

function AutomobilesList() {
  const [automobiles, setAutomobiles] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
    <h1 style={{ marginTop: "20px" }}>Automobiles</h1>
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Vin</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Availability</th>
            </tr>
        </thead>
        <tbody>
            {automobiles.map((automobile) => {
                const soldStyle = automobile.sold ? {color: "red" } : {color: "limegreen" };
                return (
                    <tr key={automobile.id}>
                        <td>{automobile.vin}</td>
                        <td>{automobile.color}</td>
                        <td>{automobile.year}</td>
                        <td>{automobile.model.name}</td>
                        <td>{automobile.model.manufacturer.name}</td>
                        <td style={soldStyle}>{automobile.sold ? "Sold" : "in stock"}</td>
                    </tr>
                );
            })}
        </tbody>
    </table>
    <a href= "http://localhost:3000/automobiles/new" role="button" className="btn btn-success">+ Add a new Automobile</a>
    </>
  );

}

export default AutomobilesList;
