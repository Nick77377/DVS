import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <a className="navbar-brand" href="/">CarCar</a>
      <div className="container-fluid">
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                id="automobileDropDown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Automobiles
              </a>
              <ul className="dropdown-menu" aria-labelledby="automobileDropDown">
                <li>
                <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
                </li>
                <li>
                <NavLink className="nav-link" to="/automobiles/new">Create an Automobile</NavLink>
                </li>
                <li>
                <NavLink className="nav-link" to="/models">Models</NavLink>
                </li>
                <li>
                <NavLink className="nav-link" to="/models/new">Add a Model</NavLink>
                </li>
                <li>
                <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
                </li>
                <li>
                <NavLink className="nav-link" to="/manufacturers/new">Create a Manufacturer</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                id="serviceDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </a>
              <ul className="dropdown-menu" aria-labelledby="serviceDropdown">
                <li>
                  <NavLink className="dropdown-item" to="/appointments">Service Appointments</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments/new">Create a Service Appointment</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/technicians">Technicians</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/technicians/new">Add a Technician</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link" to="/appointments/history">Service History</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                id="customerDropDown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Customers
              </a>
              <ul className="dropdown-menu" aria-labelledby="customerDropDown">
                <li>
              <NavLink className="nav-link" to="/customer/list">List Customers</NavLink>
                </li>
                <li>
              <NavLink className="nav-link" to="/customer">Add a Customer</NavLink>
                </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/"
                id="salesDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="salesDropdown">
                <li>
                  <NavLink className="dropdown-item" to="/sale/list">List Sales</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/sale">Add a Sale</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salesperson/list">List Salespeople</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salesperson">Add a Salesperson</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salesperson/history">Salesperson History</NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
