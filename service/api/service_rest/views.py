from service_rest.models import Technician, Appointment, Status
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianEncoder, AppointmentEncoder


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_technician(pk):
        count, _ = Technician.objects.filter(id=pk).delete()
        if count > 0:
            return JsonResponse({"deleted": True})
        else:
            return JsonResponse(
                {"message": "Technician not found"},
                status=404
            )


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            status, _ = Status.objects.get_or_create(name="active")
            appointment = Appointment(
                date_time=content["date_time"],
                reason=content["reason"],
                technician=technician,
                status=status,
                vin=content["vin"],
                customer=content["customer"],
            )
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "failed to create appointment"},
                status=400,
            )

@require_http_methods(["DELETE"])
def delete_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.delete()
        return JsonResponse(
            {"message": "Appointment deleted"}
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Appointment not found"},
            status=404
        )

@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.cancel()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.finish()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )
