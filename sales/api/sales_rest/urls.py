from django.urls import path
from .views import (
    api_list_salespersons,
    api_show_salespersons,
    api_list_customers,
    api_show_customers,
    api_list_sales,
    api_show_sales
)

urlpatterns = [
    path("salespeople/", api_list_salespersons, name="api_salespersons"),
    path("salespeople/<int:pk>/", api_show_salespersons, name="api_salesperson"),
    path("customers/", api_list_customers, name="api_customers"),
    path("customers/<int:pk>/", api_show_customers, name="api_customer"),
    path("sales/", api_list_sales, name="api_sales"),
    path("sales/<int:pk>/", api_show_sales, name="api_sale")
]
