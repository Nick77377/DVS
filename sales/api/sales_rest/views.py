from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Salesperson, Customer, Sale
from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SaleListEncoder,
)
from django.http import JsonResponse
import json
import requests

# Create your views here.


def update_sold(vin):
    automobile_api_url = (
        f"http://project-beta-inventory-api-1:8000/api/automobiles/{vin}/"
    )
    sold_status = {"sold": True}
    try:
        response = requests.put(automobile_api_url, json=sold_status)
        response.raise_for_status()
        return True
    except requests.exceptions.RequestException as e:
        print(f"Failed to update 'sold' status: {e}")
        return False


@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespersons},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the salesperson"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def api_show_salespersons(request, pk):
    if request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted:": count > 0}, status=200 if count > 0 else 404)
    else:
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson not found"})
            response.status_code = 404
            return response



@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
            status=200,
        )


@require_http_methods(["DELETE"])
def api_show_customers(request, pk):
    if request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted:": count > 0}, status=200 if count > 0 else 404)


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        serialized_sales = []
        for sale in sales:
            serialized_sale = {
                "id": sale.id,
                "price": str(sale.price),
                "automobile": sale.automobile.vin,
                "salesperson": f"{sale.salesperson.first_name} {sale.salesperson.last_name}",
                "customer": f"{sale.customer.first_name} {sale.customer.last_name}",
                "salesperson_id": sale.salesperson.id,
            }
            serialized_sales.append(serialized_sale)
        return JsonResponse(
            {"sales": serialized_sales},
            encoder=SaleListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            vin = content["automobile"]
            automobilevo_vin = AutomobileVO.objects.get(vin=vin)
            price = content["price"]
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            customer = Customer.objects.get(id=content["customer"])
            sale = Sale.objects.create(
                price=price,
                automobile=automobilevo_vin,
                salesperson=salesperson,
                customer=customer,
            )

            success = update_sold(vin)

            serialized_sale = {
                "id": sale.id,
                "price": str(sale.price),
                "automobile": sale.automobile.vin,
                "salesperson": f"{sale.salesperson.first_name} {sale.salesperson.last_name}",
                "customer": f"{sale.customer.first_name} {sale.customer.last_name}",
                "sold": True if success else False,
            }

            return JsonResponse(
                serialized_sale,
                encoder=SaleListEncoder,
                safe=False,
            )
        except (
            AutomobileVO.DoesNotExist,
            Salesperson.DoesNotExist,
            Customer.DoesNotExist,
        ):
            return JsonResponse(
                {"error": "Could not create the sale"},
                status=404,
            )


@require_http_methods(["DELETE"])
def api_show_sales(request, pk):
    if request.method == "DELETE":
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted:": count > 0}, status=200 if count > 0 else 404)
