from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale


class AutoMobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_vin", "import_sold"]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id"]

    def get_extra_data(self, o):
        return {
            "Django ID": o.id,
        }


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "phone_number", "address"]

    def get_extra_data(self, o):
        return {
            "Customer ID": o.id,
        }


class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price", "id", "salesperson_id"]
    encoders = {
        "automobile": AutoMobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "Sale ID": o.id,
        }
